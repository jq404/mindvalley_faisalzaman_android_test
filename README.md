# Mindvalley Pinterest Project

This project aims to provide an image loading library that will be used to asynchronously download the images for the pins on the pinboard when they are needed. The library is also useful for all other parts of the app where asynchronous remote image loading is required. This library is a general purpose and not assume anything about the use case, the pinboard is an example but other parts of the app that show images can also use it (e.g. a user's profile pic on the profile screen).

##Features
--------
- Make **asynchronous** HTTP requests, handle responses in **anonymous callbacks**
- HTTP requests happen **outside the UI thread**
- IMAGES and JSON are cached 
- Added "pull to refresh” feature to reload the Pinboards
- Drawer Layout added to load Activities.  

## Quick Setup

#### 1. Include library <mindvalleyloaderlibin> project 

#### 2. Android Manifest
``` xml
<manifest>
	<uses-permission android:name="android.permission.INTERNET" />
	...
	<application android:name="MyApplication">
		...
	</application>
</manifest>
```

## Usage

#### 1. Basic:

Create a new AsyncHttpClient instance:
``` java
AsyncHttpClient client = new AsyncHttpClient();
```

#### 2. Request with handlers

Make a request for JSONObject or JSONArray result with **JsonHttpResponseHandler**:
``` java
        client.get("www.example.url", POJO, Array, Context, new JSONHttpResponseHandler(){
            @Override
            public void onSuccess(ArrayList result) {

               // parse json response
            }
        });
```


#### 3. Request with handlers

Make a request for XML result with **XMLHttpResponseHandler**:
``` java
        client.get(getApplicationContext(), new XMLHttpResponseHandler<Class>(
                GET,
                "www.example.url",
                Class,
                new Response.Listener<Note>()
                {
                    @Override
                    public void onResponse(Class response) {
                         // handle XML response
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ));
```

In order to use handler for POJO's as result first create appropriate objects per GSON documentation using JSON field naming support. Example:
``` java
public class Person {

	@SerializedName("name")
	private String name;

	@SerializedName("age")
	private int age;

	// Empty constuctor needed by GSON
	public Person() {
		this.name = "";
		this.age = -1;
	}
}
```

```