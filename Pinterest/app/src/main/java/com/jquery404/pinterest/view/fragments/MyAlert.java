package com.jquery404.pinterest.view.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;

import com.jquery404.pinterest.R;

public class MyAlert extends DialogFragment{

    public static MyAlert instance = null;
    private String title="", msg="";

    public MyAlert setDialog(String title, String msg){
        this.title = title;
        this.msg = msg;

        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setNegativeButton(R.string.drawer_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setPositiveButton(R.string.menu_home, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        Dialog dialog = builder.create();


        return dialog;
    }
}
