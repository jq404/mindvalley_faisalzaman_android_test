package com.jquery404.pinterest.model.utils;

/**
 * Created by vision on 4/25/2016.
 */
public class Constants {

    public static final String BASE_URL = "http://services.hanselandpetal.com";
    public static final String BOARD_URL = BASE_URL + "/feeds/flowers.json";
    public static final String PHOTO_URL = BASE_URL + "/photos/";

    public static final String XML_URL = "http://www.w3schools.com/xml/note.xml";
}
