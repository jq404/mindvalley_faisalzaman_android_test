package com.jquery404.pinterest.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.jquery404.mindvalleyloaderlib.http.AsyncHttpClient;
import com.jquery404.mindvalleyloaderlib.http.JSONHttpResponseHandler;
import com.jquery404.mindvalleyloaderlib.http.XMLHttpResponseHandler;
import com.jquery404.pinterest.R;
import com.jquery404.pinterest.model.adapter.BoardAdapter;
import com.jquery404.pinterest.model.data.Note;
import com.jquery404.pinterest.model.data.PinBoard;
import com.jquery404.pinterest.model.utils.Constants;
import com.jquery404.pinterest.view.fragments.MyAlert;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView recyclerView;

    SwipeRefreshLayout swipeRefreshLayout;
    AsyncHttpClient client;
    List<PinBoard> pinBoards = new ArrayList<>();
    BoardAdapter boardAdapter;
    Activity mActivity;
    int current = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mActivity = this;
        client = new AsyncHttpClient();
        configViews();
        loadJson();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, R.string.menu_credit, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void configViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());


        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(
                2,
                StaggeredGridLayoutManager.VERTICAL);
        boardAdapter = new BoardAdapter(pinBoards);
        recyclerView.setAdapter(boardAdapter);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        ItemDecoration decoration = new ItemDecoration(10);
        recyclerView.addItemDecoration(decoration);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJson();
            }
        });
    }



    private void loadXml() {

        client.get(getApplicationContext(), new XMLHttpResponseHandler<Note>(
                Request.Method.GET,
                Constants.XML_URL,
                Note.class,
                new Response.Listener<Note>()
                {
                    @Override
                    public void onResponse(Note response) {
                        swipeRefreshLayout.setRefreshing(false);
                        String markup = "From : "+response.getFrom() + "\n"
                                +"To: "+ response.getTo()+ "\n"
                                +"Message: "+ response.getBody()+ "\n";

                        MyAlert alert = new MyAlert().setDialog(response.getHeading(), markup);
                        alert.show(getFragmentManager(), "Alert");
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ));
    }

    private void loadJson()
    {

        final ArrayList<PinBoard> pinBoard = new ArrayList<>();

        client.get(Constants.BOARD_URL, PinBoard.class, pinBoard, getApplicationContext(), new JSONHttpResponseHandler(){
            @Override
            public void onSuccess(ArrayList result) {
                swipeRefreshLayout.setRefreshing(false);
                int start = 0, end = 5;
                for (int i = start; i < end; i++) {
                    boardAdapter.addPinboard((PinBoard) result.get(i));
                    start = end;
                    end = 10;
                }
                recyclerView.scrollToPosition(0);
            }
        });

    }


    private void loadAbout() {
        String markup = "Hi there ! \nI am Faisal Zaman.\nI design and develop games in various platforms";

        MyAlert alert = new MyAlert().setDialog("About", markup);
        alert.show(getFragmentManager(), "Alert");
    }

    private void loadContact() {
        String markup = "If you like to know more about me follow me on\n" +
                "Twitter @jquery404";

        MyAlert alert = new MyAlert().setDialog("Contact", markup);
        alert.show(getFragmentManager(), "Alert");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Toast.makeText(getApplicationContext(), "Home", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_json) {
            loadJson();
        } else if (id == R.id.nav_xml) {
            loadXml();
        } else if (id == R.id.nav_cache) {

        } else if (id == R.id.nav_about) {
            loadAbout();
        } else if (id == R.id.nav_contact) {
            loadContact();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
