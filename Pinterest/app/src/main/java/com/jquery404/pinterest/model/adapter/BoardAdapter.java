package com.jquery404.pinterest.model.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jquery404.pinterest.R;
import com.jquery404.pinterest.model.data.PinBoard;
import com.jquery404.pinterest.model.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;


public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.Holder>{

    List<PinBoard> pinBoards;

    public BoardAdapter(List<PinBoard> pinBoards)
    {
        this.pinBoards = pinBoards;
    }

    public void addPinboard(PinBoard pinBoard)
    {
        pinBoards.add(pinBoard);
        notifyItemInserted(0);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new Holder(row);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        PinBoard currentPin = pinBoards.get(position);

        holder.mName.setText(currentPin.getName());
        Picasso.with(holder.itemView.getContext())
                .load(Constants.PHOTO_URL + currentPin.getPhoto())
                .into(holder.mImage);
    }

    @Override
    public int getItemCount() {
        return pinBoards.size();
    }

    class Holder extends RecyclerView.ViewHolder
    {
        public TextView mName;
        public ImageView mImage;

        public Holder(View itemView)
        {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.pin_title);
            mImage = (ImageView) itemView.findViewById(R.id.pin_image);
        }

    }
}
