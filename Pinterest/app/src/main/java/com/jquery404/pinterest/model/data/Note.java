package com.jquery404.pinterest.model.data;

public class Note
{
    private String to;

    private String body;

    private String from;

    private String heading;

    public String getTo ()
    {
        return to;
    }

    public void setTo (String to)
    {
        this.to = to;
    }

    public String getBody ()
    {
        return body;
    }

    public void setBody (String body)
    {
        this.body = body;
    }

    public String getFrom ()
    {
        return from;
    }

    public void setFrom (String from)
    {
        this.from = from;
    }

    public String getHeading ()
    {
        return heading;
    }

    public void setHeading (String heading)
    {
        this.heading = heading;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [to = "+to+", body = "+body+", from = "+from+", heading = "+heading+"]";
    }
}