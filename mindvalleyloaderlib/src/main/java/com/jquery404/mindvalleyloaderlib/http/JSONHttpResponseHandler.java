package com.jquery404.mindvalleyloaderlib.http;


import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;


public class JSONHttpResponseHandler {



    public ArrayList jsonParser(JSONArray jsonArray, Class myClass, ArrayList data) {

        Gson g = new Gson();
        JSONArray json = jsonArray;
        try {

            for (int i = 0; i < json.length(); i++) {
                data.add(g.fromJson(json.getString(i), myClass));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }



    public void onSuccess(ArrayList content)
    {
        onSuccess(content);
    }


}
