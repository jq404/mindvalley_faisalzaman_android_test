package com.jquery404.mindvalleyloaderlib.http;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.jquery404.mindvalleyloaderlib.controller.CacheRequest;
import com.jquery404.mindvalleyloaderlib.controller.Controller;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AsyncHttpClient {


    RequestQueue requestQueue;

    void AsyncHttpClient()
    {
    }

    public void get(String url,
                    final Class myClass,
                    final ArrayList array,
                    final Context context,
                    final JSONHttpResponseHandler responseHandler)
    {
        requestQueue = Controller.getInstance(context).getRequestQueue();

        CacheRequest cacheRequest = new CacheRequest(0, url,
                new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JSONArray res = new JSONArray(jsonString);

                    responseHandler.jsonParser(res, myClass, array);
                    responseHandler.onSuccess(array);

                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        cacheRequest.setRetryPolicy(
                new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Controller.getInstance(context).addToRequestQueue(cacheRequest);

        requestQueue.add(cacheRequest);
    }

    public void get(final Context context, final XMLHttpResponseHandler responseHandler)
    {
        requestQueue = Controller.getInstance(context).getRequestQueue();
        requestQueue.add(responseHandler);
    }

}
